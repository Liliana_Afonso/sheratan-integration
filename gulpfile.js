// on déclare nos dépendances gulp
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps'); // sert à dire ou est le fichier css correspondant dans le web tool
var autoprefixer = require('gulp-autoprefixer'); // sert à faire automotiquement les webkit (plus besoin de faire webkit-...)
var browserSync = require('browser-sync').create(); // sert à automatiser le refresh

// sass utilise le compiler de nodejs
sass.compiler = require('node-sass');

// création de ma tâche pour compiler mes fichiers sass en css
gulp.task('sass', function (){
    return gulp.src('./src/sass/**/*.scss') // va chercher tous les fichiers scss dans le dossier sass
    .pipe(sourcemaps.init())
    .pipe(sass({
        errLogToConsole: true,
        outputStyle: 'compressed'
    })) // minify en un fichier mais si erreur alors écrit dans la ligne de commande l'erreur
    .pipe(autoprefixer({
        overrideBrowserslist: ['> 0%', 'IE 11'],
        cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./build/css/')) // si pas erreur alors ca créer le fichier dans build/css en css
    .pipe(browserSync.stream());
});

// On crée une fonction pour mettre tous les fichiers html dans build pour ensuite que le css soit pris en compte dans le browserSync
gulp.task('html', function (){
    return gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'))
});

// on a crée une tâche pour mettre les imges dans build/static
gulp.task('assets', function (){
    return gulp.src('./src/static/**/**/*.*')
        .pipe(gulp.dest('./build/static'))
});

// on a crée une tâche js pour mettre les fichiers et dossiers du dossier js dans build/js
gulp.task('js', function (){
    return gulp.src('./src/js/**/*.*')
        .pipe(gulp.dest('./build/js'))
});

// On crée un watcher pour surveiller les changements sur les fichiers (terminal reste ouvert)
gulp.task('watch', function () {
    browserSync.init({
        server: {
            baseDir: './build'
        }
    })
    gulp.watch('./src/sass/**/*.scss', gulp.series('sass'));
    gulp.watch('./src/*.html', gulp.series('html')).on('change', browserSync.reload);

});

// One task to build all task
// il suffira de écrire gulp pour lancer gulp sass, gulp html et gulp watch
gulp.task('default', gulp.parallel('sass', 'html', 'assets', 'js', 'watch'));