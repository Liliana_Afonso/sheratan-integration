/*  Carousel */
$('.sliders').slick({
    dots: true,
    infinite: true,
    speed: 300,
    fade: true,
    arrows:false,
    autoplay:true,
    cssEase: 'linear'
});

/* mobile menu */
function on() {
    console.log('on');
    document.getElementById("overlay").style.display = "block";
    document.querySelector(".menu-mobile").style.left = "0";
    document.querySelector("html>body").style.overflow = "hidden";

}

function off() {
    console.log('off');
    document.getElementById("overlay").style.display = "none";
    document.querySelector(".menu-mobile").style.left = "-1000px";
    document.querySelector("html>body").style.overflow = "inherit";

}